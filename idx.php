<?php
/**
 *
 *  Copyright 2008, Arthur Richards arthur@colingolabs.com
 * 
 *  This is an API to help handle IDX feeds
 *  
 *  This file is part of the MLS module for Drupal.
 *
 *  The MLS module is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The MLS module is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MLS module.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

class IDX{
	private $mls;
	private $user;
	
	/**
	 * IDX constructor
	 *
	 * @param object $mls
	 *   This should be a loaded mls-feed node
	 * @param object $user
	 */
	function __construct($mls,$user){
		set_time_limit(0);
		$this->mls = $mls;
		//$user = $this->user;
    $this->user = user_load(array('uid'=>$user->uid));
	}
	
	/**
	 * Updates IDX files locally.
	 * 
	 * Uses wget to fetch files from IDX server, and then unzips them.
	 *
	 * @param string $location
	 * @param string $dir
	 *   a temporary holding directory for unzipped IDX files
	 */
	function update($location,$dir){
		if(file_exists($dir)){
			exec('rm -rf ' . $dir);
		}
		exec('mkdir ' . $dir);
		watchdog('mls','Downloading IDX info: ' . $location);
		exec('wget -O ' . $dir . '/idx.zip ' . $location);
		watchdog('mls','Unzipping IDX info: ' . $location);
		$out = exec('unzip ' . $dir . '/idx.zip -d ' . $dir);
		watchdog('mls','output from unzip: ' . $out);
	}
	
	/**
	 * Instantiates a node object.
	 *
	 * @param string $title
	 * @param string $type
	 * @return object
	 *   The newly created node object
	 */
	function initialize_node($title,$type){
	  //build our new node
	  $node = new StdClass();
	  $node->type = $type;
	  $node->uid = $this->user->uid;
	  $node->name = $this->user->name;
	  $node->title = $title; 
	  $node->body = '';
	  $node->teaser = '';
	  $node->status = 1;
	  if(isset($this->mls->og_groups)){
	    foreach($this->mls->og_groups as $gid){
	      $node->og_groups[] = $gid;
	    }
	  }
	
	  
	  return $node;
	}
	
	/**
	 * Attaches fields from IDX to the node.
	 *
	 * @param object $node
	 * @param string $csv_line
	 *   A line from IDX file
	 * @param array $field_map
	 *   A mapping of our DB fields to the IDX fields
	 */
	function attach_fields(&$node,$csv_line,$field_map){
	  foreach($field_map as $field => $index){

	    $node->$field = array(array('value'=>str_replace("--"," - ",htmlentities($csv_line[$index],ENT_COMPAT,'UTF-8',FALSE))));
	  }
	  //override status and set to active.
	  $node->field_status = array(array('value'=>'Active'));
	    //set to active
    $node->field_active = array(array('value'=>1));
	}
	
	/**
	 * Saves node
	 *
	 * @param object $node
	 */
	function commit_node(&$node){
	  //commit the node.
	  node_save($node);
	}
	
	/**
	 * Fetches all listing #s currently in DB for this feed
	 *
	 * @return array
	 *   An array of all the listing numbers, where listing numbers are keys
	 */
	function get_current_listings($class){
		$class_list = implode("','",$class);
	  $current_listings = array();
	  $result = db_query("SELECT field_listing_number_value FROM {content_type_mls_listing} WHERE field_mls_id_nid = %d AND field_class_value IN ('$class_list')",$this->mls->nid);
	  while($row = db_fetch_object($result)){
	    $current_listings[$row->field_listing_number_value] = TRUE;
	  }
	  return $current_listings;
	}
	
	/**
	 * Handles removal of stale listing from our DB
	 *
	 * //TODO handle images
	 * 
	 * @param array $current_listings
	 *   Array of listings fetched from current IDX file
	 * @param array $existing_listings
	 *   Array of listings that existed in DB prior to parsing IDX file
	 */
	function remove_stale_listings($current_listings,$existing_listings){
	  //remove nonexistant listings from db
		foreach(array_keys($current_listings) as $listing){
		  if(!isset($existing_listings[$listing])){
		    $listings_to_remove[] = $listing;
		  }
		}
		if($listings_to_remove){
		  $remove = implode("','",$listings_to_remove);
		  $result = db_query("SELECT nid FROM {content_type_mls_listing} WHERE field_listing_number_value IN ('$remove') AND field_mls_id_nid = %d",$this->mls->nid);
		  while($row = db_fetch_object($result)){
		    mls_node_delete($row->nid);
		    db_query("DELETE FROM {node_comment_statistics} WHERE nid = %s",$row->nid);
		    
		    //delete associated images
		    $images = db_query("SELECT nid,field_path_value FROM {content_type_mls_image} WHERE field_listing_id_nid = %d",$row->nid);
		    while($image_row = db_fetch_object($images)){
		    	mls_node_delete($image_row->nid);
		    	db_query("DELETE FROM {node_comment_statistics} WHERE nid = %s",$image_row->nid);
		    	if(file_exists($image_row->field_path_value)){
		    	  unlink($image_row->field_path_value);
		    	}
		    }
		  }
		}
		
		return;
	}
	
	/**
	 * Handles mls_image node creation
	 * 
	 * Moves IDX images from tmp directory to
	 * a directory specified for the feed.
	 *
	 * @param string $temp_dir
	 * @param string $file
	 * @param object $node
	 * @param int $index_number
	 */
	function image_handler($temp_dir,$file,$node,$index_number){
    $new_image = $this->mls->field_image_path[0]['value'] . '/' . $file;
    if(file_exists($new_image)){
    	return;
    }elseif(copy($temp_dir . '/' . $file, $new_image)){
      $image_node = $this->initialize_node($node->title . ' - Image','mls_image');

      //attach image node values
      $image_node->field_image_id = array(array('value'=>$node->field_listing_number[0]['value']));
      $image_node->field_index_number = array(array('value'=>$index_number));
      $image_node->field_path = array(array('value'=>$new_image));
      $image_node->field_url = array(array('value'=>'DIRECT'));
      $image_node->field_listing_id = array(array('nid' => $node->nid,'error_field' => 'field_listing_id][nids'));
      
      $this->commit_node($image_node);
      watchdog('mls','Image ' . $image_node->title . ' created.');
    }
	}
	
	function shutdown(){
		if(!is_null($e = error_get_last())){
			if($e['type'] == E_ERROR){
				$error = 'Fatal error in ' . $e['file'] . ' at line ' . $e['line'] . ': ' . $e['message'];
			  watchdog('mls',$error);
			  mail('awjrichards@gmail.com','IDX error',$error);
			}
		}
	}
}
?>