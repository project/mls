<?php
/**
 * 
 *  Copyright 2008, Arthur Richards arthur@colingolabs.com
 * 
 *  This file is part of the MLS module for Drupal.
 *
 *  The MLS module is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The MLS module is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MLS module.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @TODO: how to handle dynamic field names for querying
 * @TODO: if pagination/batches happenning automatically by viele, need to fix limits here.
 */

define('MAIN_DIRECTORY', variable_get('mls_viele_path','.'));
define('COMMON_DIRECTORY', MAIN_DIRECTORY . '/common');

/**
 * constructs master filename for archival purposes.
 *
 * @param unknown_type $data_file
 * @return unknown
 */
function _mls_make_master_data_file_name($data_file){
	return dirname($data_file) . '/' . basename($data_file,'.xml') . '_' . date('Y-m-d') . '.xml';
}

/**
 * Runs the first import from vieleRETS.
 * 
 * @param int $nid
 * @return string
 *   The full path to the data file that we can run the parser on
 */
function _mls_first_run($nid){
	ini_set('memory_limit','128M');
	$start = date('Y-m-d H:i:s');
	watchdog('mls','Downloading MLS listings began at ' .$start);
	//echo 'start: ' . $start . '<br />';
	//flush();
	
	//set the basic query values.
	$query_values = array(
	 'Status' => '|A',
	 'MLNumber' => '0+',
	 'ZipCode' => '*',
	);
	
  //fetch the bcf
  $bcf = _mls_get_batch_control_file($nid);
  
  //create/update bcf
  _mls_create_bcf($bcf,$query_values);	

  //download the listings
  $master_file = _mls_download_listings($bcf,$nid,1);

  //set the first run variable for this mls feed so we know we've done this before!
  variable_set('mls_first_run_'.$nid,date('c'));
  return $master_file;
}

/**
 * downloads mls listings
 *
 * Because of import limitations of some MLS providers, we need a way to ensure
 * that we get all initial active listings.  So we execute extracts until we get
 * no more listings.
 *
 * @TODO last update bug on multiple iterations when just updating.  no timestamp being passed in.
 * 
 * @param string $bcf
 * @param int $nid
 * @param bool $is_first
 * @return
 */
function _mls_download_listings($bcf,$nid,$is_first=NULL){
	//set time limit to 0 so we don't time out.
  set_time_limit(0);

  //set the basic query values.
  $query_values = array(
   'Status' => ($is_first) ? '|A' : 'ANY',
   'MLNumber' => '0+',
   'ZipCode' => '*',
  );
  
  //check to see if there was a 'last run' variable in the bcf
  $original_last_run = NULL;
  $sx = simplexml_load_file(MAIN_DIRECTORY . '/batch_control_files/' . $bcf);
  if($sx->LAST_RUN){
  	$original_last_run = (string) $sx->LAST_RUN;
  }
  
  //determine last run time for BCF
  $last_run = date("Y-m-d\TH:i:s");
    
  //get our data file
  $data_file = _mls_get_data_file($nid);

  //open up our master xml file that will be populated with XML from mls feed
  $master_file = _mls_make_master_data_file_name($data_file);
  if(is_file($master_file)){
  	unlink($master_file);
  }
  $fh = fopen($master_file,'wb');
  fwrite($fh,"<?xml version=\"1.0\"?>\n<VIELE>"); 
  
  //now that we have our first set of data, we need to read through it and grab the last mlnumber.
  //start by getting the field map.
  $map = _mls_get_field_map($nid);
  if(!$map){
  	drupal_set_message(t('Could not fetch field mapping'),'error');
    return '';
  }
  
  //set $index to something greater than -1 for our while loop evaluation
  $index = 0;
  while($index > -1){
    //echo ' . ';
    //flush();
    
    //once we have our BCF, we should run the job.
  	_mls_run_viele($bcf,$data_file);
    
    //we have to strip out extraneous tags otherwise it will cause xml parsing to barf later.
    //perhaps this can be done more elegantly?
    $dfh = fopen($data_file,'rb');
    $file = fread($dfh,filesize($data_file));
    fclose($dfh);
    $file = str_replace(array("<?xml version=\"1.0\"?>","<VIELE>","</VIELE>"),'',$file);
    
    //write the data file created by vieleRETS to our master file.
    fwrite($fh,$file,filesize($data_file));
    
    //open up the data file as simplexml for easy parsing.
    $sx = simplexml_load_file($data_file);
    if(!$sx){
    	drupal_set_message('Failed to load Viele XML file for some reason','error');
    	return ''; 
    }
    //if there are no listings, break the loop.
    if(!$sx->LISTING){
      $index = -1;
      continue;
    }
    
    //get the number of listings, subtract 1 for the index we need of the last $sx->LISTING 
    //$index = count($sx->LISTING) - 1;
    
    //get the last MLNumber
    $listings = array();
    foreach($sx->LISTING as $listing){
    	$listings[] = (int)$listing->MLNumber;
    }
    $mlnumber = max($listings);//(int)$sx->LISTING[$index]->MLNumber;
    
    //set the new MLNumber for the query values
    $query_values['MLNumber'] = ($mlnumber + 1) . '+';
    
    //update the bcf with new query values
    if($is_first){
      _mls_create_bcf($bcf,$query_values);
    }else{
    	//$bcf,$query_values=NULL,$with_timestamp=FALSE,$update_only=FALSE,$update_bcf=FALSE,$timestamp_value=NULL
    	_mls_create_bcf($bcf,$query_values,(isset($original_last_run)) ? TRUE : FALSE,TRUE,FALSE,$original_last_run);
    }
    
    //echo ' . ';
    //flush();
  }
  
  fwrite($fh,"</VIELE>");
  fclose($fh);
  
  $downloaded = date('Y-m-d H:i:s');
  //echo ' <br />' . 'finished downloading: ' . $downloaded . '<br />';

  
  //now we need to update the bcf to be update only, with all MLNumbers, and include our original timestamp.*/
  $query_values['MLNumber'] = '0+';
  $query_values['Status'] = 'ANY';
  _mls_create_bcf($bcf,$query_values,TRUE,TRUE,TRUE,$last_run);
  
  //finally, we're done.
  $end = date('Y-m-d H:i:s');
  watchdog('mls','Finished downloading MLS listings at ' . $end);
  //echo  '<br />' . 'end: ' . $end . '<br />';
  //echo 'Memory usage: ' . memory_get_peak_usage(TRUE);
  
  return $master_file;
}

/**
 * Creates/updates a bcf.
 * 
 * The query paramters must be 'Status', 'MLNumber' and 'ZipCode' for now.
 * 
 * @param string $bcf the filename of your bcf
 * @param array $query_values an array of query values, the key is the field name the value is... the value.
 */
function _mls_create_bcf($bcf,$query_values=NULL,$with_timestamp=FALSE,$update_only=FALSE,$update_bcf=FALSE,$timestamp_value=NULL){
	
	//include viele files so we can easily manipulate the BCF
  require_once(COMMON_DIRECTORY . '/controller.php');
  require_once(COMMON_DIRECTORY . '/model.php');
  require_once(COMMON_DIRECTORY . "/view.php");
  require_once(MAIN_DIRECTORY . '/common/extract.php');

  //instantiate the extract class
  $extract = new Extract();
	
  //create a new BCF parser so we can fetch the query values
  $bcf_parser = new BCFParser();

  //read the BCF
  $bcf_parser->parse(file_get_contents(MAIN_DIRECTORY . "/batch_control_files/" . $bcf));
  
  if(!$query_values){
  	$query_values = array(
      'Status' => '|A',
      'MLNumber' => '0+',
      'ZipCode' => '*',
    );
  }
  
  //update the bcf
  $extract->createControlFile(
      $bcf,
      '1000',
      $bcf_parser->extractName,
      $query_values,
      $with_timestamp,
      NULL,
      $update_only,
      $update_bcf,
      $timestamp_value,
      TRUE,
      NULL
    );
  
}

/**
 * Runs vieleRETS run_interactive_job.php given a batch control file.
 *
 * @param string $bcf
 *   filename of the batch control file you wish to execute
 */
function _mls_run_viele($bcf,$original_data_file){
	//we first need to CD to the main directory otherwise the scripts fail on us.  also, in case of corruption, we delete the original data file.
  $main_dir = MAIN_DIRECTORY;
	`cd $main_dir; rm $original_data_file; php -q ./run_interactive_job.php batch_control_files/$bcf`;
	return;
}

/**
 * Executes a Viele extract
 * 
 * @param object $mls
 * @param bool $init
 */
function _mls_execute_extract($mls){
	require_once('mls.module');
  set_time_limit(0);
  //prepare the field mapping
  $map = db_result(db_query("SELECT field_map FROM {mls_field_map} WHERE nid = %d",$mls->nid));
  if(!$map){
    drupal_set_message(t('Failed to fetch field mapping.'),'error');
    return '';
  }
  
  //unserialize the map
  $map = unserialize($map);

	//check if we've ever run an import before.  if we have, just run the update.  otherwise, execute the first run
 	//echo $mls->nid;exit;
  if(variable_get('mls_first_run_'.$mls->nid,FALSE)){
 		  $data_file = _mls_download_listings(_mls_get_batch_control_file($mls->nid),$mls->nid);
 	  //$data_file = "/home/arthur/Dev/vieleRETS/mls_data/sfarmls/listings/sfarmls_2008-10-20.xml";
 	}else{
 		//execute the first run method which returns our data file.
 		$data_file = _mls_first_run($mls->nid);
 	}
 
 	//if we didnt get an actual data file to parase back, return;
 	if(!$data_file){
 		drupal_set_message(t('MLS execution halted: no data file.'),'error');
 		return '';
 	}

 	//parse the xml file
 	_mls_parse_listings($data_file,$mls->nid);
  /*if(!$parse){
  	drupal_set_message(t('There was an error parsing the listings.'),'error');
    return '';
  }*/
  
  drupal_set_message(t('MLS feed '.$mls->title.' successfuly updated!'));
  
  //update city/state vars
  _mls_update_vars();
  watchdog('mls',t('MLS vars updated.'));
  watchdog('mls',t('Updates completed for ' . $mls->title . '.'));
  
  return;
}

/**
 * Fetches data file for parsing
 *
 * @param int $nid
 * @return strng
 *   data file location
 */
function _mls_get_data_file($nid){
	//find the location of the target
  $target = _mls_get_viele_target($nid);

  //find the location of the XML file containing the dump
  $data_file = _mls_get_viele_listing_xml($target);
  
  return $data_file;
}

/**
 * Fetches the stored field mappings for a particular mls service.
 *
 * @param int $nid
 * @return array The array of field mappings.
 */
function _mls_get_field_map($nid){
	//fetch field mappings
  $map = db_result(db_query('SELECT field_map FROM mls_field_map WHERE nid = %d',$nid));
   
  //if there's no mapping, we can't proceed. return with an error.
  if(!$map){
    drupal_set_message(t('Failed to fetch field mapping.  Fields must be mapped for import to work.'),'error');
    return 0;
  }
  
  //the mappings come back in a serialized array of viele_field => drupal_field, we want drupal_field => viele_field 
  return array_flip(unserialize($map));
}

/**
 * Parses XML file of listings from VieleRETS
 *
 * @param string $data_file
 *   full path to file containing XML of listings from VieleRETS
 * @return bool
 */
function _mls_parse_listings($data_file,$nid){ 
	GLOBAL $GLOBALS;

	if(!is_file($data_file)){
  	drupal_set_message(t('No data file was passed to the MLS parser.','error'));
  	return '';
  }
	
  //make sure we're not collecting query information - this will cause memory problems.
  if(isset($GLOBALS['conf']['dev_query'])){
		$GLOBALS['conf']['dev_query'] = 0;
	}
  
	watchdog('mls','MLS listing processing begins.');

	//fetch existing listings
  $existing_listings = _mls_get_existing_listings($nid);
  if(!$existing_listings){
    return 0;
  }elseif($existing_listings == -1){
  	$existing_listings = array();
  }
  
  //get the title of the feed.  this will be used to help construct the title of listing/image nodes
  //$feed_title = db_result(db_query('SELECT title FROM {node} WHERE nid = %d',arg(2)));
  //load the mls feed node
  $mls = node_load($nid,NULL,TRUE);

  //get the field mappings
  $map = _mls_get_field_map($mls->nid);
  
  //get the listing class
  $class = _mls_get_viele_class($mls->nid);
  if(!$class){
  	drupal_set_message("No listing class was found fo " . $mls->title,'error');
  	return 0;
  }
  
  //open up the data file with XMLReader for easy parsing
  $reader = new XMLReader();
  $reader->open($data_file);
  
  //create a way to tell if we have any listings
  $listing_count = 0;

  //read the file
  while($reader->read()){
  	
  	//create an empty listing array to store data in
  	$listing = array();
  	$images = array();
  	
  	//check to see if we're at a listing.
  	if($reader->nodeType == XMLReader::ELEMENT && $reader->localName == 'LISTING'){
  		
  		//record that we have at least one listing
  		$listing_count = $i++;
  		
  		//set an image iterator
  		$image_iterator = 0;
  		
  		//iterate through the listing. 
  		while($reader->read()){
  			switch($reader->nodeType){ 			

  				// if we're at the closing 'LISTING tag, break the switch and current while and move on.  
  				case XMLReader::END_ELEMENT && $reader->localName == 'LISTING':
  					break 2;

  				case XMLReader::ELEMENT && $reader->localName == 'IMAGE':
  					while($reader->read()){
  						
  						//if we hit the closing IMAGE element, break out of the whiel and move one.
  						if($reader->nodeType == XMLReader::END_ELEMENT && $reader->localName == 'IMAGE'){
  							break;
  						}
  						
  						//if we're at an end element, move on in the while
  						if($reader->nodeType == XMLReader::END_ELEMENT){
  							continue;
  						}
  						
  					  //store the value of the text node in this tree
              if($reader->nodeType == XMLReader::TEXT){
                $images[$image_iterator][$current_image_tree] = $reader->value;
                continue;
              }
  						
  						//store the node name for the current tree
  						if($reader->nodeType == XMLReader::ELEMENT){
  							$current_image_tree = $reader->localName;
  							$images[$image_iterator][$current_image_tree] = 1; 
  							continue;
  						}
  					}
  					
  					//update the image iterator
  					$image_iterator++;
  					break;	
  					
 					// if we're at a text node, 
  				case XMLReader::TEXT:
  					//echo $reader->value;exit;
            $listing[$current_tree] = $reader->value;
  					break;
 						
 					//if we're simply at a non-text element, fetch the local name.
  				case XMLReader::ELEMENT:
  					//echo $reader->localName . ': ';
  					$current_tree = $reader->localName;
  					$listing[$current_tree] = 1;
  					break;
  					
  				//if we're at an end element we should move on.
          case XMLReader::END_ELEMENT:
            break;
  					
  				default:
  					break;
  			}
  		}
  		
  		//make sure that at least the listing number is set.  if it's not, go to next iteration of while.
  		if(!isset($listing[$map['field_listing_number']])){
  			continue;
  		}
  		$listing_number = $listing[$map['field_listing_number']];
  	    
  		//check to see if this listing already exists
	    if(isset($existing_listings[$listing_number])){
	      
	      //if the listing exists, make sure that the last modified time stamp is newer than what we have on file before we process this
	      if(strtotime($listing[$map['field_last_modified']]) <= strtotime($existing_lisitngs[$listing_number]['modified'])){
	      	continue;
	      }
	    
	      //load the node to update
	      $node = node_load($existing_listings[$listing_number]['nid'],NULL,TRUE);
	    }
	        
	    //if a node hasn't been loaded, create one.
	    if(!$node){
	      $node = _mls_create_node('mls_listing',$mls->title . ' - ' . $listing_number,$mls);
	    }
	
	    //attach the feed's nid to the node
	    $node->field_mls_id[0] = array('nid' => $nid,'error_field' => 'field_mls_id][nids');
	    //attach the class
	    $node->field_class = array(array('value' => $class));
	    //attach field names and values to the node
	    foreach($map as $drupal_field => $value){
	      //we have to cast the $listing object value as string, otherwise it stores it as an object
	      $node->$drupal_field = array(array('value' => $listing[$value]));
	    }
	    
      //turn field_active to true.
      $node->field_active = array(array('value' => 1));
	    

	    //var_dump($node);
	    //save the node.
	    _mls_node_commit($node);
	    
	    //update the existing listngs to include details of this listing
	    $existing_listings[$listing_number]['modified'] = $node->field_last_modified[0]['value'];//(string)$listing->$map['field_last_modified'];
	    $existing_listings[$listing_number]['nid'] = $node->nid;

	  	//handle images
	    if(isset($images)){
	      $nodes = array();
	      
	      //check to see if there are images associated with this node.  if there are, delete them and start over.
	      $result = db_query('SELECT nid FROM {content_type_mls_image} WHERE field_listing_id_nid = %d', $node->nid);
	      if(db_num_rows($result) > 0){
	        while($row = db_fetch_object($result)){
            //call our custom node_delete function
	        	mls_node_delete($row->nid);
	        }
	      }
	      
	      //create nodes for each of the images
	      foreach($images as $image){
	        //unset $image_node if it's set
	        if(isset($image_node)) unset($image_node);
	        
	        //create the actual image node
	        $image_node = _mls_create_node('mls_image', $mls->title . ' - ' . $listing_number . ' - ' . $image['INDEX']);
	        
	        //attach the index field
	        $image_node->field_index_number = array(array('value' => $image['INDEX']));
	        
	        //attach the listing id
	        $image_node->field_listing_id[0] = array('nid' => $node->nid,'error_field' => 'field_listing_id][nids');
	        
	        //attach the path
	        $image_node->field_path = array(array('value' => $image['PATH']));
	        
	        //attach the URL
	        $image_node->field_url = array(array('value' => $image['URL']));

	        //commit the image node
	        _mls_node_commit($image_node);
	      }
	      unset($node);
	      unset($image_node);
	    }
  	}
  }
    
  //if there are no listings, we need to throw an error and return.
  if(!$listing_count){
  	drupal_set_message(t('No listings available for import.'));
  }else{
  	drupal_set_message(t($listing_count . ' listing(s) processed.'));
  }
    
  //store the existing listings in the cache.
  if(is_array($existing_listings)){
    variable_set('listings_' . $mls->nid,serialize($existing_listings));
  }
  
  //clear the mls search cache
  cache_clear_all('*','cache_mls',1);
  
  watchdog('mls','Peak memory consumption during listing processing ' . memory_get_peak_usage(TRUE));
  watchdog('mls','MLS listing processing completed.');
  return;
}

/**
 * Creates nodes for mls module
 *
 * @param unknown_type $type
 * @param unknown_type $title
 * @param unknown_type $mls
 * @return object
 */
function _mls_create_node($type,$title,$mls = NULL){
  global $user;

  if(!isset($user->uid) || !isset($user->name)){
	  $user = user_load(array('uid'=>$user->uid));
  }
  if(!isset($user->uid) || $user->uid != 1){
  	$user->uid = 1;
  	$user->name = 'admin';
  }
  
  //establish our new node
  $node = new StdClass();
  $node->type = $type;
  $node->uid = $user->uid;
  $node->name = $user->name;
  $node->title = $title; 
  $node->body = '';
  $node->teaser = '';
  $node->status = 1;
  if(isset($mls->og_groups)){
    foreach($mls->og_groups as $gid){
      $node->og_groups[] = $gid;
    }
  }
  unset($user);
  return $node;
}

/**
 * Commits mls-related nodes
 *
 * @param unknown_type $node
 */
function _mls_node_commit(&$node){
  node_save($node);
  cache_clear_all('content:'. $node->nid .':'. $node->vid, 'cache_content');
}

/**
 * Gets existing listings for a feed and their last modified times
 *
 * @param int $feed_id
 * @return array
 */
function _mls_get_existing_listings($feed_id = NULL){
  //ensure that we have an nid for a feed before we proceed
  if(!$feed_id){
    if(arg(2)){
      $feed_id = arg(2);
    }else{
      //if we've got nothing, throw an error and return.
      drupal_set_message(t('No feed NID has been provided.'),'error');
      return 0;
    }
  }
  
  $existing_listings = array();
  //check the cache to see if we have the listings set already for this feed.  if it's not, then we have to build the array.
  $result = variable_get('listings_' . $feed_id,0);
  if($result){
    $result = unserialize($result);
    return $result;
  }else{  
    $result = db_query('SELECT nid, field_listing_number_value, field_last_modified_value FROM {content_type_mls_listing} WHERE field_mls_id_nid = %d',$feed_id);
    //set to -1 so we know later that this worked, but there are just no results
    if(!db_num_rows($result)){
    	return -1;
    }
    while($row = db_fetch_object($result)){
      $existing_listings[$row->field_listing_number_value]['modified'] = $row->field_last_modified_value;
      $existing_listings[$row->field_listing_number_value]['nid'] = $row->nid;
    }

    return $existing_listings;
  }
}


/**
 * Fetches extract file fro a given mls feeld
 *
 * @param int $nid
 * @return string
 *   filename for the viele batch file, including the path.
 */
function _mls_get_viele_extract($nid){
	//fetch the bcf
  $bcf = _mls_get_batch_control_file($nid);
  
  $bcf_full_path = MAIN_DIRECTORY . "/batch_control_files/" . $bcf;
  
  if(!file_exists($bcf_full_path)){
  	drupal_set_message('No BCF found!','error');
  	drupal_goto('mls/field_map');
  }
  
  //load the viele bcf xml
  $sx = simplexml_load_file($bcf_full_path);
  
  //grap the extract file and create its path
  $extract = MAIN_DIRECTORY . '/extracts/' . trim($sx->EXTRACT);
  
  return $extract;  
}

/**
 * Fetch the BCF
 *
 * @param int $nid
 * @return string
 *   BCF file name
 */
function _mls_get_batch_control_file($nid){
	return db_result(db_query("SELECT field_batch_file_value FROM {content_type_mls_feed} WHERE nid = %d",$nid));
}

/**
 * Fetches the target file path from a given mls feed
 *
 * @param int $nid
 * @return string
 *   filenam for the viele target, including path
 */
function _mls_get_viele_target($nid){
  //find the location of the extract
  $extract = _mls_get_viele_extract($nid);
  
  //read through extract file and get the location of the target
  $fh = fopen($extract,'r');
  
  //iterate through the extract until we find the line with target
  while (!isset($target)){
    $line = trim(fgets($fh));
    if(substr($line,0,15) == 'define("TARGET"'){
      //turn the column list into an array
      $target = substr($line,17,-3);
    }   
    
    //this is here in case the column list isn't found, we don't want to loop 4eva
    if(feof($fh)) $target = 0;
  }
  //close the file
  fclose($fh);
  
  //throw an error if no valid viele target found
  if(!$target){
    drupal_set_message('Unable to find a Viele target.','error');
    return '';
  }
  
  //get the viele path
  $target = MAIN_DIRECTORY . '/targets/' . trim($target);
  return $target;
}

/**
 * For a given target, locate the file containing XML of listings
 *
 * @param string $target
 *   this must be a full path to the target file
 * @return string
 *   full path to the xml file containing listings
 */
function _mls_get_viele_listing_xml($target){
  //open the target file
  $fh = fopen($target,'r');
  
  //iterate through target file until we get the lines for download path and file name.
  while (!isset($found)){
    $line = trim(fgets($fh));
    if(substr($line,0,18) == 'define("FILE_NAME"'){
      $filename = substr($line,20,-3);
    }
    if(substr($line,0,27) == 'define("DATA_DOWNLOAD_PATH"'){
      $path = substr($line,29,-3);
    }
    
    if(isset($path) && isset($filename)) $found = 1;
    if(feof($fh)) $found = 0;
  }
  
  //now that we have the filename and path, we can construct the full path to data file
  $data_file = $path . '/' . $filename;

  return $data_file;
}

/**
 * Fetches the listing class for an mls feed
 *
 * @param int $nid
 * @return string
 *   the class
 */
function _mls_get_viele_class($nid){
  $source = _mls_get_viele_source(_mls_get_viele_extract($nid));
  
  if(!$source){
  	drupal_set_message('Source file for Viele extract ' . $extract . ' does not exist.','error');
  	return 0;
  }
  
  $fh = fopen($source,'r');
  
  while(!isset($found)){
  	$line = trim(fgets($fh));
  	if(substr($line,0,24) == 'define("SELECTION_CLASS"'){
  		$class = substr($line,26,-3);
  		$found = 1;
  	}
  	if(feof($fh)) $found = 0;
  }
  
  if(!$found){
  	return 0;
  }else{
  	return $class;
  }
}

/**
 * Fetches the 'source' file for viele from an extract.
 *
 * @param string $extract
 * @return string
 *   full path to source
 */
function _mls_get_viele_source($extract){
 $fh = fopen($extract,'r');
  
  while(!isset($found)){
    $line = trim(fgets($fh));
    if(substr($line,0,15) == 'define("SOURCE"'){
      $source = MAIN_DIRECTORY . '/sources/' . substr($line,17,-3);
      $found = 1;
    }
    if(feof($fh)) $found = 0;
  }
  if(!$found || !file_exists($source)){
    return 0;
  }else{
  	return $source;
  }
}

/**
 * Fetches fields from the viele extract file as well as drupal db 
 * to allow us to map them together.
 *
 * @TODO errors/error handling
 * @return array
 *   associative array - $arr['viele'] holds an array of viele fields, $arr['drupal'] holds an array of drupal fields.
 */
function _mls_fetch_mls_fields_for_map(){
  //fetch available fields associated with mls listings from database
  $mls_fields = array();
  $result = db_query(
    "SELECT 
      field_name, 
      label 
    FROM 
      {node_field_instance} 
    WHERE 
      field_name LIKE '%s' AND 
      field_name NOT LIKE '%s' AND
      type_name = '%s'
    ORDER BY label ASC
    ",
    'field_%',
    '%_nid',
    'mls_listing'
  );
  //store the fields in an array, $arr[field_name] = label
  while($row = db_fetch_object($result)){
    if($row->label == 'active') continue;//this is for internal control so we don't want to present this as an option to the user   
    $mls_fields[$row->field_name] = $row->label;
  }
  
  //now we have to fetch the field names from the extract.
  $extract = _mls_get_viele_extract(arg(2));
  
  //open the extract file
  $fh = fopen($extract,'r');
  
  //iterate through the extract until we find the line with the column list
  while (!isset($viele_fields)){
    $line = trim(fgets($fh));
    if(substr($line,0,20) == 'define("COLUMN_LIST"'){
      //turn the column list into an array
      $viele_fields = explode(",",substr($line,22,-3));
    }   
    
    //this is here in case the column list isn't found, we don't want to loop 4eva
    if(feof($fh)) $viele_fields = 0;
  }
  fclose($fh);
  return array('drupal' => $mls_fields,'viele' => $viele_fields);
}
?>