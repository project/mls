
MLS Module
------------------------

~Installation and Setup~
To install, place the entire MLS folder into your modules directory.
Go to administer -> site building -> modules and enable the MLS module.

Now go to administer -> MLS Module Settings. Fill in the full system path
to your installation of VieleRETS 1.1.6.  Also be sure to fill in the
path to where you will keep any IDX handling scripts.

There is currently a strange bug with the 'dependent' content fields.
Fields declared to be dependent on another field for a content type do not
get carried over when the content type is exported for use in a module.
So you'll need to masage some fields in the 'MLS Feed' content type:

  1) Go to administer -> content management -> content types
  2) Select 'MLS Feed'
  3) Select 'Manage Fields'
  4) Select 'configure' for the 'Protocol' field
  5) Click 'Save field settings' (yes that's all you do here ;)
  6) Select 'configure' for the 'Batch Control File' field
  7) Select 'RETS option ...' from the 'Field instances' box
  8) Click 'Save field settings'
  9) Do steps 6-8 for 'IDX Image Path' and 'IDX Script Path' fields, but
     select 'IDX option ...' from the 'Field instances' box.
  10) Click 'Update' on the MLS Feed field management page

~Usage~

There are two user permissions - 'manage mls feeds' and 'administer mls
module'.  The former allows users to add, edit, delete and execute MLS
feeds.  The latter allows a user to actually configure the module.  Make sure
to assign these roles to the apropriate users.

For VieleRETS:

First you'll need to set up your VieleRETS installation and create a source,
target and extract.  This module expects the MLS Listing Number to be the
unique identifier for listings, so make sure that you include the MLS listing
number as a field to be fetched from the MLS.  Also be sure to select a
'last updated' field so the module knows what listings have been updated since
the feed was last executed.

VERY IMPORTANT: When creating your 'target', be sure to select 'XML Formatted
File' for the target type.  Also, make sure the directory for images is somewhere
above the root directory that your web server has access to (ie the 'files' 
directory in your Drupal installation).

Once you've created a source, target and extract, execute your extract to 
make sure everything works and to create a batch control file.  I recomend that
you tell Viele to only fetch one listing - otherwise this might take a while.

You are now ready to configure your MLS feed in Drupal.  Go to create content ->
MLS Feed.  Give your MLS Feed a title, make sure 'RETS' is selected under
'protocol' and be sure to enter the Batch Control File name (I believe it's 
the same as the name of your extract).  You just need to enter the file name,
not the full path.

Once you have an MLS Feed content type set up, you will need to map the fields
from VieleRETS to Drupal.  Go to adminster -> content management -> administer
mls feeds.  Find the feed you just added in the table and select 'Map fields'.
The labels above the dropdowns correspond to field names retrieved from the MLS.
Select the corresponding Drupal CCK field from the dropdown.  If a corresponding
field does not exist, you might want to create a CCK field for the MLS Feed 
content type.  Once you're done mapping the fields, click 'submit'.  Congrats, 
your fields are mapped and you're ready to fetch your listings!

You can then manually click 'execute' in order to have your feed run.  It might
take a while so be patient.  You can also set up cron jobs to do this automagically
which will be covered below.

For IDX:

IDX is a truly tricky beast.  This module provides a kind of API in idx.php that a
developer can leverage in order to incorporate listings fetched from an IDX service
with your Drupal installation.  Take a look in idx.php.  If it doesn't make sense to
you (it might not -interfacing with CCK is tricky and confusing by nature), please
feel free to contact me.  If there's enough interest and I have spare time (ha!) I'll
write a how-to for making use of the API.

Once you have a script that will fetch the IDX listings and push them into Drupal, just
set up the MLS feed in Drupal in a similar way as for VieleRETS.  create content ->
mls feed.  Give it a title, select 'IDX' as your protocol, enter the FULL path to where
the images will be stored, and enter the path to the IDX scripts, relative to what's set
in the MLS module settings.  (Also, if there's enough interest this might get cleaned up
to be more inutitive - or feel free to do it yourself and send me a patch!)

~How updating listings works~
The first time you execute an MLS feed, this module fetches all listings marked as 'Active'
from the MLS.  Every other time a feed is executed, it looks for anything that has been
modified (and added) after the last time the feed was executed.  That way, any 'Active'
listings that are no longer active get deactivated in the database.

~Cron for automating MLS feed updates~
This module comes with its own cron script.  There are many different reasons for not using
the default cron script/hooks - email me if you want to hear them.  You should copy
mls_cron.php into the top level of your Drupal installation.  Then, create a cronjob to 
hit mls_cron.php (just like you would hit Drupal's cron.php) every so often.  The module
is set to only allow a feed to update once every 24 hours.  If you have multiple feeds, 
it's advisable to have your cron job execute fairly frequently.  I built this module for
a company that currently has a dozen or so MLS feeds (and will be adding more) and I have
the cronjob executing every 15 minutes to check and see if it's time to update the listings.



~Contact~
http://www.colingolabs.com
arthur [at] colingolabs.com
